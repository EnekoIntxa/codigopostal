"""codigopostal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Views.HomeView import HomeView
from API.views import API

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", HomeView.home, name="home"),
    path('registrarNombre/',API.index,name="registrarNombre"),
    path("listarNombres/",API.listar_nombres,name="listarNombres"),
    path("eliminarNombres/<int:id_nombre>",API.delete,name="eliminarNombres"),
    path("confirmareliminacion/<int:id_nombre>",API.confirm_delete,name="confirmar_eliminacion"),
    path("elegirvista/",API.elegir_vista,name="elegirvista")
]

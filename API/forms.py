from django import forms
from API.models import Formulario


class FormularioCodigoPostal(forms.ModelForm):
    class Meta:
        model = Formulario
        fields = ['id','nombre','cp']

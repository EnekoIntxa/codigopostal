import requests
from bs4 import BeautifulSoup
from django.http import HttpResponse

def funcion_ciudad(cp):
    cp=str(cp)
    API_ENDPOINT = f"http://www.geonames.org/postalcode-search.html?q={cp}&country=ES"

    req=requests.get(API_ENDPOINT)

    soup = BeautifulSoup(req.text, 'html.parser')
    table = soup.find(class_='restable')

    try:
        line = table.find_all('tr')[1]
        line_split=line.contents[6].text.split("\xa0")

        if len(line_split)>4:
            ciudad=line_split[0]
        else:
            ciudad=line.contents[1].text
    except:
        ciudad=None
    return ciudad

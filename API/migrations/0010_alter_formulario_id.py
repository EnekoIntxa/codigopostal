# Generated by Django 3.2 on 2021-04-19 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0009_alter_detalle_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulario',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]

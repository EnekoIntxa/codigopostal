from django.db import models

# Create your models here.

# Creación de campos de la tabla
class Formulario(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=100)
    cp = models.CharField(max_length=5) #Codigo postal
    ciudad = models.CharField(max_length=100)

    class Meta:
         db_table = 'BBDD' # Le doy de nombre 'Datos' a nuestra tabla en la Base de Datos


class Master(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=100)
    class Meta:
         db_table = 'master'

class Detalle(models.Model):
    id = models.IntegerField(primary_key=True, unique=True)
    cp = models.CharField(max_length=5) #Codigo postal
    ciudad = models.CharField(max_length=100)

    class Meta:
         db_table = 'detalle'

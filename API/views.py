from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from API.forms import FormularioCodigoPostal
from API.models import Formulario, Master, Detalle
from API.funciones import funcion_ciudad



# Create your views here.

class API(HttpResponse):
    def index(request):
        formulario = FormularioCodigoPostal()
        return render(request,"NombreIndex.html",{"form":formulario})

    def listar_nombres(request):
        formulario = Formulario.objects.all()
        return render(request,"ListaNombres.html",{"formulario":formulario})


    def delete(request,id_nombre):
        nombre = Formulario.objects.get(pk=id_nombre)
        nombre.delete()
        nombre = Master.objects.get(pk=id_nombre)
        nombre.delete()
        nombre = Detalle.objects.get(pk=id_nombre)
        nombre.delete()
        formulario = Formulario.objects.all()
        return render(request,"ListaNombres.html",{"formulario":formulario,"mensaje":"OK"})

    def confirm_delete(request,id_nombre):
        return render(request,"ConfirmDelete.html",{"id_nombre":id_nombre})

    def elegir_vista(request):
        formulario = FormularioCodigoPostal(request.POST)
        if formulario.is_valid():
            ciudad = funcion_ciudad(formulario.cleaned_data["cp"])
            if ciudad!=None:
                id = formulario.cleaned_data["id"]
                nombre = formulario.cleaned_data["nombre"]
                cp = formulario.cleaned_data["cp"]
                observacion = Formulario(id=id,nombre=nombre, cp=cp, ciudad=ciudad)
                observacion.save()
                observacion = Master(id=id,nombre=nombre)
                observacion.save()
                observacion = Detalle(id=id,cp=cp,ciudad=ciudad)
                observacion.save()

                formulario = FormularioCodigoPostal()

                if "Guardar" in request.POST:

                    return render(request,"NombreIndex.html", {"form":formulario,"request":request.POST,"mensaje":"OK"})


                elif "JSON" in request.POST:
                    responseData = {'result':'OK'}

                    return JsonResponse(responseData)
            else:
                formulario = FormularioCodigoPostal()
                return render(request,"NombreIndex.html", {"form":formulario,"request":request.POST,"cpincorrect":"Codigo postal incorrecto"})
        else:
            return render(request,"NombreIndex.html", {"form":formulario,"request":request.POST})
